# Loadtest using gatling


Get the project
---------------

```bash
git clone https://ashokballolli@bitbucket.org/ashokballolli/loadtestgatling.git

```

Start SBT
---------
```bash
$ sbt
```

Run all simulations
-------------------

```bash
> gatling:test
```

Run a single simulation
-----------------------

```bash
> gatling:testOnly com.gatling.user.BasicTests
```
