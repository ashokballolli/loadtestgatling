package com.gatling.user

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

// https://gorest.co.in/public-api/users?_format=json&access-token=Ugnrl0va7T-6DteuCIL0_LqaOmNQPIcyV5X8
class BasicTests extends Simulation {
  private val baseUrl = " https://gorest.co.in"
  //  private val basicAuthHeader = "Basic YmxhemU6UTF3MmUzcjQ="
  //  private val authPass = "Q1w2e3r4"
  private val contentType = "application/json"
  private val endpoint = "/public-api/users?_format=json&access-token=Ugnrl0va7T-6DteuCIL0_LqaOmNQPIcyV5X8"
  private val requestCount = 1

  val httpProtocol: HttpProtocolBuilder = http
    .baseUrl(baseUrl)
    .inferHtmlResources()
    .acceptHeader("*/*")
    //    .authorizationHeader(basicAuthHeader)
    .contentTypeHeader(contentType)
    .userAgentHeader("automation")

  //  val headers_0 = Map("Expect" -> "100-continue")
  val queryParams = Map("_format" -> "json",
    "access-token" -> "Ugnrl0va7T-6DteuCIL0_LqaOmNQPIcyV5X8")

  val scn: ScenarioBuilder = scenario("loadtestUsers")
    .exec(http("getUsers")
      .get(endpoint)
      //      .headers(headers_0)
      //      .basicAuth(authUser, authPass)
      .check(status.is(200)))

  setUp(scn.inject(atOnceUsers(requestCount))).protocols(httpProtocol)
}
