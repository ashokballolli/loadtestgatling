package com.gatling

import com.gatling.user.GetPosts
import io.gatling.app.Gatling
import io.gatling.core.config.GatlingPropertiesBuilder
// https://github.com/puppetlabs/gatling-puppet-load-test/blob/master/simulation-runner/src/main/scala/com/puppetlabs/gatling/runner/PuppetGatlingRunner.scala
/**
 * This object simply provides a `main` method that wraps
 * [[io.gatling.app.Gatling]].main, which
 * allows us to do some configuration and setup before
 * Gatling launches.
 */
object Runner {

  def main(args: Array[String]) {

//    val config = PuppetGatlingConfig.initialize()

    // This sets the class for the simulation we want to run.
    val simClass = classOf[GetPosts].getName

    val props = new GatlingPropertiesBuilder
    props.resourcesDirectory("./src/main/scala")
    props.binariesDirectory("./target/scala-2.11/classes")
    props.simulationClass(simClass)
//    props.runDescription(config.runDescription)




    // This checks the values set in gatling_kickoff.rb
//    if (sys.env("PUPPET_GATLING_REPORTS_ONLY") == "true") {
//      props.reportsOnly(sys.env("PUPPET_GATLING_REPORTS_TARGET"))
//    }

    Gatling.fromMap(props.build)

  }
}
